package com.example.demo;

import io.github.bucket4j.Bucket;
import io.github.bucket4j.ConsumptionProbe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleController {

    private static final Logger log = LoggerFactory.getLogger(SampleController.class);

    @Autowired protected RateLimiter rateLimiter;

    /**
     * Simple sample endpoint to implement bucket4j rate-limiter 😉
     * The concept:
     * - store token based on a key, at this case I use path variable {id} (using pathVariable since its easier implementation 😝)
     * - if the token of an {id} is has no quota, then it's un-consumable till bucket4j generate new token for the {id}
     * Simple, right?
     * @param id
     * @return new Model([String])
     */
    @GetMapping(path = "/message/{id}")
    public Model getMessage(@PathVariable("id") String id) {
        Bucket bucket = rateLimiter.resolveBucket(id);
        ConsumptionProbe probe = bucket.tryConsumeAndReturnRemaining(1);

        if(probe.isConsumed()) {
            return new Model("Hello world!");
        } else {
            long remainingTimeToRefill = probe.getNanosToWaitForRefill() / 1_000_000_000;
            String errorResult = "Wait time to refill the token: " + remainingTimeToRefill + " second(s)";
            log.error(errorResult);
            return new Model(errorResult);
        }
    }
}
