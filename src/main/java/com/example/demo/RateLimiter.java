package com.example.demo;

import io.github.bucket4j.Bandwidth;
import io.github.bucket4j.Bucket;
import io.github.bucket4j.Refill;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class RateLimiter {

    /**
     * this variable will contains any new token based on id
     * on more advanced topic, you can use hazelcast or redis, depend on your requirements ;)
     */
    Map<String, Bucket> bucketCache = new ConcurrentHashMap<>();

    public Bucket resolveBucket(String id) {
        return bucketCache.computeIfAbsent(id, this::newBucket);
    }

    private Bucket newBucket(String id) {
        return Bucket.builder()
                .addLimit(Bandwidth.classic(3, Refill.intervally(3, Duration.ofMinutes(1))))
                .build();
    }
}
