package com.example.demo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class SampleControllerTest extends BaseTest {

    private static final String MOCK_ID_1 = "111";
    private static final String MOCK_ID_2 = "222";
    private static final String MOCK_ID_3 = "333";
    private static final String SUCCESS_RESPONSE_SAMPLE = "Hello world!";
    private static final String ERROR_RESPONSE_SAMPLE = "second(s)";
    private static final String CONTROLLER_BASE_ENDPOINT = "/message/";

    @Autowired
    private MockMvc mockMvc;


    @Override
    @BeforeEach
    void setUp() {

    }

    @Test
    public void testSampleControllerEndPoint() {
        try {
            this.mockMvc.perform(get(CONTROLLER_BASE_ENDPOINT + MOCK_ID_1))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(content().string(containsString(SUCCESS_RESPONSE_SAMPLE)));

            this.mockMvc.perform(get(CONTROLLER_BASE_ENDPOINT + MOCK_ID_1))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(content().string(containsString(SUCCESS_RESPONSE_SAMPLE)));

            this.mockMvc.perform(get(CONTROLLER_BASE_ENDPOINT + MOCK_ID_1))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(content().string(containsString(SUCCESS_RESPONSE_SAMPLE)));

            this.mockMvc.perform(get(CONTROLLER_BASE_ENDPOINT + MOCK_ID_1))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(content().string(containsString(ERROR_RESPONSE_SAMPLE)));
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSampleControllerEndPointWithMultipleUsersShouldReturnExpectedValue() {
        try {
            this.mockMvc.perform(get(CONTROLLER_BASE_ENDPOINT + MOCK_ID_3))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(content().string(containsString(SUCCESS_RESPONSE_SAMPLE)));

            this.mockMvc.perform(get(CONTROLLER_BASE_ENDPOINT + MOCK_ID_3))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(content().string(containsString(SUCCESS_RESPONSE_SAMPLE)));

            this.mockMvc.perform(get(CONTROLLER_BASE_ENDPOINT + MOCK_ID_3))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(content().string(containsString(SUCCESS_RESPONSE_SAMPLE)));

            this.mockMvc.perform(get(CONTROLLER_BASE_ENDPOINT + MOCK_ID_2))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(content().string(containsString(SUCCESS_RESPONSE_SAMPLE)));
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
}
