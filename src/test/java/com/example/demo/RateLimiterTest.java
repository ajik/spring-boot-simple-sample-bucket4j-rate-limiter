package com.example.demo;

import io.github.bucket4j.Bucket;
import io.github.bucket4j.ConsumptionProbe;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class RateLimiterTest extends BaseTest {

    private static final String MOCK_ID_1 = "111";
    private static final String MOCK_ID_2 = "222";

    private RateLimiter rateLimiter;

    @Override
    @BeforeEach
    public void setUp() {
        rateLimiter = new RateLimiter();
        rateLimiter.resolveBucket(MOCK_ID_1);
    }

    @Test
    public void addNewTokensShouldReturnExpectedResult() {
        rateLimiter.resolveBucket(MOCK_ID_2);

        Map<String, Bucket> buckets = rateLimiter.bucketCache;
        assertThat(buckets.size()).isEqualTo(2);
        assertThat(buckets.get(MOCK_ID_1)).isNotNull();
        assertThat(buckets.get(MOCK_ID_2)).isNotNull();
    }

    @Test
    public void consumeTokenShouldReturnExpectedResult() {
        Map<String, Bucket> buckets = rateLimiter.bucketCache;

        Bucket bucket = buckets.get(MOCK_ID_1);

        assertThat(bucket).isNotNull();

        ConsumptionProbe probe = bucket.tryConsumeAndReturnRemaining(1);
        assertThat(probe.isConsumed()).isTrue();
        probe = bucket.tryConsumeAndReturnRemaining(1);
        assertThat(probe.isConsumed()).isTrue();
        probe = bucket.tryConsumeAndReturnRemaining(1);
        assertThat(probe.isConsumed()).isTrue();
        probe = bucket.tryConsumeAndReturnRemaining(1);
        assertThat(probe.isConsumed()).isFalse();

    }

    @Test
    public void consumeTokenMultipleUserShouldReturnExpectedValue() {
        rateLimiter.resolveBucket(MOCK_ID_2);

        Map<String, Bucket> buckets = rateLimiter.bucketCache;

        Bucket bucket = buckets.get(MOCK_ID_1);
        Bucket bucket2 = buckets.get(MOCK_ID_2);

        assertThat(bucket).isNotNull();

        ConsumptionProbe probe = bucket.tryConsumeAndReturnRemaining(1);
        assertThat(probe.isConsumed()).isTrue();
        probe = bucket.tryConsumeAndReturnRemaining(1);
        assertThat(probe.isConsumed()).isTrue();
        probe = bucket.tryConsumeAndReturnRemaining(1);
        assertThat(probe.isConsumed()).isTrue();
        probe = bucket.tryConsumeAndReturnRemaining(1);
        assertThat(probe.isConsumed()).isFalse();

        ConsumptionProbe probe2 = bucket2.tryConsumeAndReturnRemaining(1);
        assertThat(probe2.isConsumed()).isTrue();
        probe2 = bucket2.tryConsumeAndReturnRemaining(1);
        assertThat(probe2.isConsumed()).isTrue();
        probe2 = bucket2.tryConsumeAndReturnRemaining(1);
        assertThat(probe2.isConsumed()).isTrue();
        probe2 = bucket2.tryConsumeAndReturnRemaining(1);
        assertThat(probe2.isConsumed()).isFalse();
    }
}
